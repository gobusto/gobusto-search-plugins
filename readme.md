Firefox Search Plugins
======================

A collection of custom search plugins I made for Firefox. It's usually possible
to install them in the standard way (via the Firefox search box) by visiting my
[website](http://gobusto.neocities.org/).

None of these are officially endorsed by/affiliated with the sites they search.

[Do what you want with them.](http://creativecommons.org/publicdomain/zero/1.0)
